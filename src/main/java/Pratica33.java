import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 3);
        double[][] m = orig.getMatriz();
        m[0][0] = 01; m[0][1] = 02; m[0][2] = 03; m[1][0] = 04;  m[1][1] = 01;
        m[1][2] = 02; m[2][0] = 03; m[2][1] = 04; m[2][2] = 05;
        
        Matriz orig_01 = new Matriz(3, 3);
        double[][] n = orig_01.getMatriz();
        n[0][0] = 02; n[0][1] = 05; n[0][2] = 04; n[1][0] = 01;  n[1][1] = 03;
        n[1][2] = 03; n[2][0] = 05; n[2][1] = 03; n[2][2] = 02;
        
        Matriz soma   = orig.soma(m,n);
        Matriz prod   = orig.prod(m,n);
        System.out.println(orig);
        System.out.println(orig_01);
        System.out.println(soma);
        System.out.println(prod);
    }
}
